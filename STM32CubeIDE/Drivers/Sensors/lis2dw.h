/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */

#ifndef _LIS2DH_H_
#define _LIS2DH_H_

#include "driver_types.h"

#define LIS2DH_INIT_STEPS   LIS2DH_INIT, LIS2DH_DONE
#define LIS2DH_INT_STEPS    LIS2DH_INIT_INT, LIS2DH_INT_DONE

typedef enum range_t {
    RANGE_2G = 0b00,
    RANGE_4G = 0b01,
    RANGE_8G = 0b10,
    RANGE_16G = 0b11
} range_t;

typedef enum datarate_t {
    DR_1HZ = 0b0001,
    DR_10HZ = 0b0010,
    DR_25HZ = 0b0011,
    DR_50HZ = 0b0100,
    DR_100HZ = 0b0101,
    DR_200HZ = 0b0110,
    DR_400HZ = 0b0111,
    DR_1620HZ = 0b1000
} datarate_t;

typedef struct {
    int16_t axis_x;
    int16_t axis_y;
    int16_t axis_z;
} lis2dw_data;

void lis2dw_init(eDeviceStatus_t* pstatus);

void lis2dw_read(eDeviceStatus_t* pstatus, lis2dw_data* pdata);

#endif
