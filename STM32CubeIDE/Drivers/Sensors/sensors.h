/*
 * sensors.h
 *
 *  Created on: Dec 16, 2022
 *      Author: User
 */

#ifndef DRIVERS_SENSORS_SENSORS_H_
#define DRIVERS_SENSORS_SENSORS_H_

#define USE_SHTx	1
#define USE_LIS2DW	1

#if (USE_SHTx == 1)
#include "sht31.h"
#endif
#if (USE_LIS2DW == 1)
#include "lis2dw.h"
#endif

#endif /* DRIVERS_SENSORS_SENSORS_H_ */
