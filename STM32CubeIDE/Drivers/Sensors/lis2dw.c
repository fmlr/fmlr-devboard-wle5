/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */
#include "i2c_if.h"
#include "lis2dw.h"

#define LIS2DW_SA0      0b1
#define LIS2DW_ADDRESS  ((0b001100 << 2) | (LIS2DW_SA0 << 1))

#define WHO_AM_I        0x0F
#define WHO_AM_I_ID     0b01000100

#define OUT_T_L         0x0D
#define OUT_T_H         0x0E

#define CTRL1                 0x20
#define CTRL2                 0x21
#define CTRL3                 0x22
#define CTRL4_INT1_PAD_CTRL   0x23
#define CTRL5_INT2_PAD_CTRL   0x24
#define CTRL6                 0x25

#define OUT_T           0x26

#define STATUS_REG      0x27

#define OUT_X_L         0x28
#define OUT_X_H         0x29

#define OUT_Y_L         0x2A
#define OUT_Y_H         0x2B

#define OUT_Z_L         0x2C
#define OUT_Z_H         0x2D

#define ACT_THS         0x3E
#define ACT_DUR         0x3F

#define FIFO_CTRL       0x2E
#define FIFO_SAMPLES    0x2F

#define TAP_THS_X       0x30
#define TAP_THS_Y       0x31
#define TAP_THS_Z       0x32

#define INT_DUR         0x33
#define WAKE_UP_THS     0x34
#define WAKE_UP_DUR     0x35

#define FREE_FALL       0x36
#define STATUS_DUP      0x37

#define WAKE_UP_SRC     0x38
#define TAP_SRC         0x39
#define SIXD_SRC        0x3A
#define ALL_INT_SRC     0x3B

#define X_OFS_USR       0x3C
#define Y_OFS_USR       0x3D
#define Z_OFS_USR       0x3E

#define CTRL7           0x3F


#define I2C_TIMEOUT     500

static struct {
    eDeviceStatus_t* pstatus;
    lis2dw_data* pdata;
    unsigned int threshold;
    unsigned char buf[8];
} lis2dw;

static uint8_t lis2dw_write_byte(uint8_t register_addr, uint8_t value)
{
	lis2dw.buf[0] = register_addr;
	lis2dw.buf[1] = value;
	return HAL_I2C_Master_Transmit(&hi2c1, LIS2DW_ADDRESS, lis2dw.buf, 2, I2C_TIMEOUT);
}

static uint8_t lis2dw_read_byte(uint8_t register_addr, uint8_t* value)
{
	lis2dw.buf[0] = register_addr;
	if (HAL_I2C_Master_Transmit(&hi2c1, LIS2DW_ADDRESS, lis2dw.buf, 1, I2C_TIMEOUT) == HAL_OK)
	{
		return HAL_I2C_Master_Receive(&hi2c1, LIS2DW_ADDRESS, value, 1, I2C_TIMEOUT);
	}
	else return HAL_ERROR;
}

static uint8_t lis2dw_read_multiple(uint8_t register_addr, uint8_t* value, uint8_t size)
{
	lis2dw.buf[0] = register_addr;
	if (HAL_I2C_Master_Transmit(&hi2c1, LIS2DW_ADDRESS, lis2dw.buf, 1, I2C_TIMEOUT) == HAL_OK)
	{
		return HAL_I2C_Master_Receive(&hi2c1, LIS2DW_ADDRESS, value, size, I2C_TIMEOUT);
	}
	else return HAL_ERROR;
}

static void init_func()
{
	uint8_t i2c_status = HAL_OK;
	uint8_t who_am_i= 0;

	// check if device is on
	i2c_status = lis2dw_read_byte(WHO_AM_I, &who_am_i);
	if (i2c_status == HAL_ERROR || who_am_i != WHO_AM_I_ID)
	{
		return;
	}

	// soft reset to defaults
	i2c_status = lis2dw_write_byte(CTRL2, 0b01000000);
	if (i2c_status == HAL_ERROR)
	{
		return;
	}

	uint8_t answer;
	do {
		i2c_status = lis2dw_read_byte(CTRL2, &answer);
		HAL_Delay(5);
	} while (answer & 0x40); //check if the reset is finished


	// with ODR = 12Hz, low power mode 0
	i2c_status = lis2dw_write_byte(CTRL1 | 0x80, 0b0010 << 4 | 0b00 << 2 | 0b00);
	if (i2c_status == HAL_ERROR)
	{
		return;
	}

	i2c_status = lis2dw_write_byte(CTRL6, 0b00 << 6 | 0b11 << 4 | 0b0 << 3 | 0b0 << 2);
	if (i2c_status == HAL_ERROR)
	{
		return;
	}

	// enable multiple byte read
	i2c_status = lis2dw_write_byte(CTRL2, 0b00000100);
	if (i2c_status == HAL_ERROR)
	{
		return;
	}

    *lis2dw.pstatus = eDevice_Initialized;
}

static void read_func()
{
    uint8_t i2c_status = HAL_OK;

    i2c_status = lis2dw_read_multiple(OUT_X_L, lis2dw.buf, 6);
    if (i2c_status == HAL_ERROR)
    {
        return;
    }

    const int32_t div = (1 << 16) / 32000;
    lis2dw.pdata->axis_x = (lis2dw.buf[0] | (lis2dw.buf[1] << 8)) / ((uint16_t)div);
    lis2dw.pdata->axis_y = (lis2dw.buf[2] | (lis2dw.buf[3] << 8)) / ((uint16_t)div);
    lis2dw.pdata->axis_z = (lis2dw.buf[4] | (lis2dw.buf[5] << 8)) / ((uint16_t)div);

    return;

}

void lis2dw_init(eDeviceStatus_t* pstatus)
{
    lis2dw.pstatus = pstatus;
    init_func();
}

void lis2dw_read(eDeviceStatus_t* pstatus, lis2dw_data* pdata)
{
    lis2dw.pstatus = pstatus;
    lis2dw.pdata = pdata;
    read_func();
}
