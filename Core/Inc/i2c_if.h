/*
 * i2c_if.h
 *
 *  Created on: Dec 20, 2022
 *      Author: User
 */

#ifndef APPLICATION_USER_CORE_I2C_IF_H_
#define APPLICATION_USER_CORE_I2C_IF_H_

#include "i2c.h"

void I2C_Register_TxCallback(void *cb);
void I2C_Register_RxCallback(void *cb);
uint8_t I2C_xfer(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData,
        uint16_t wSize, uint16_t rSize, uint16_t timeout);
void I2C_resume();


#endif /* APPLICATION_USER_CORE_I2C_IF_H_ */
