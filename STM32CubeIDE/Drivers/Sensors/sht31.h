/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */

#ifndef _SHT31_H_
#define _SHT31_H_

#include "driver_types.h"

typedef struct {
    int32_t temp;
    int32_t hum;
} sht31_data;

void sht31_init(eDeviceStatus_t* pstatus);

void sht31_read(eDeviceStatus_t* pstatus, sht31_data* pdata);

#endif
